// tabs
function SuperMegaUltraTabificator(widget) {
  this.widget = widget;
  this.buttons = this.widget.getElementsByClassName('js-tab-widget__trigger');
  this.contents = this.widget.getElementsByClassName('js-tab-widget__content');
  this.attach();
}
SuperMegaUltraTabificator.prototype.cleanClasses = function huiak() {
  for (var i = 0; i < this.buttons.length; i++) {
    this.contents[i].classList.remove("is-active");
    this.buttons[i].classList.remove("is-active");
  }
};
SuperMegaUltraTabificator.prototype.clickAction = function huuiak(clickEvent) {
  var index = clickEvent.currentTarget.getAttribute('data-index');
  clickEvent.preventDefault();
  this.cleanClasses();
  this.widget.querySelector('.js-tab-widget__trigger[data-index="'+ index +'"]').classList.add('is-active');
  this.widget.querySelector('.js-tab-widget__content[data-index="'+ index +'"]').classList.add('is-active');
};
SuperMegaUltraTabificator.prototype.attach = function production() {
  for (var i = 0; i < this.buttons.length; i++) {
    this.buttons[i].addEventListener("click", this.clickAction.bind(this));
  }
};
document.addEventListener("DOMContentLoaded", function() {
  var tabWidgets = document.getElementsByClassName("js-tab-widget");
  for (var j = 0; j < tabWidgets.length; j++) {
    new SuperMegaUltraTabificator(tabWidgets[j]);
  }
});

// url
function parseURL(string) {
  return new URL(string);
}
