var gulp = require('gulp'),
  autoprefixer = require('gulp-autoprefixer'),
  browserSync = require('browser-sync'),
  filter = require('gulp-filter'),
  sass = require('gulp-sass'),
  twig = require('gulp-twig'),
  sourcemaps = require('gulp-sourcemaps'),
  reload = browserSync.reload,
  src = {
    css: './css/',
    sass: './scss/{,*/}*.{scss,sass}',
    js: 'js/',
    templates: './templates/{,*/}*.twig'
  };

'use strict';

gulp.task('templates', function () {
  return gulp.src('./index.twig')
    .pipe(twig())
    .pipe(gulp.dest('./'))
    .on("end", reload);
});

gulp.task('sass-dev', function () {
  return gulp.src(src.sass)
    .pipe(sourcemaps.init())
    .pipe(sass({
      errLogToConsole: true
    }))
    .on('error', function (err) {
      console.error('Error!', err.message);
    })
    .pipe(autoprefixer({browsers: ['last 2 versions']}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(src.css))
    .pipe(filter("**/*.css"))
    .pipe(reload({
      stream: true
    }));
});

gulp.task('sass-prod', function () {
  return gulp.src(src.sass, { follow: true })
    .pipe(sass({
      errLogToConsole: true
    }))
    .on('error', function (err) {
      console.error('Error!', err.message);
    })
    .pipe(autoprefixer({browsers: ['last 2 versions']}))
    .pipe(gulp.dest(src.css))
    .pipe(filter("**/*.css"))
});

gulp.task('dev', ['sass-dev', 'templates'], function () {
  browserSync({
    xip: true,
    server: {
      baseDir: "./"
    },
    files: [src.css, src.js, src.templates],
    injectChanges: true,
    reloadDelay: 100
  });
  gulp.watch(src.sass, ['sass-dev']);
  gulp.watch(src.javascript, [reload]);
  gulp.watch(src.templates, ['templates', reload]);
});

gulp.task('default', ['dev']);
gulp.task('prod', ['sass-prod', 'templates']);
